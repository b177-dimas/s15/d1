// Operators

// Assignment operators
// Basic assignment operator (=)
let assignmentNumber = 8;

// Arithmetic Assignment operators
// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment operator (-=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

// Mutliplication assignment operator (*=)
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

// Division assignment operator (/=)
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of remainder operator: " + remainder);

// Multiple Operators and Parentheses

// Using MDAS
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parentheses
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment (++) and Decrement (--)
let z = 1;
// Pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrement
decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

let numD = false + 1;
console.log(numD);

// Relational operators

// Equality operator(==)

let juan = 'juan';

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); // true
console.log('juan' == juan);

// Inequality operator(!=)
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log('juan' != juan); //true

// <, >, <=, >=
console.log("Less than and greater than");
console.log(4 < 6); //true
console.log(2 > 8); //false
console.log(5 >= 5); //true
console.log(10 <= 15); //true

// Strict equality operator (===)
console.log("Strict equality:");
console.log(1 === '1'); //false

// Strict inequality operator (!==)
console.log("Strict inequality:");
console.log(1 !== '1'); //true

// Logical operators
let isLegalAge = false;
let isRegistered = false;

// AND Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// OR Operator (||)
// Return true if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection control structures

// if, else if and else statement

// if statement
// Executes a statement if a specified condition is true
/* Syntax:
	if(condition){
		statement/s;
	}
*/

let numE = -1;

if(numE < 0){
	console.log("Hello");
}

let nickName = 'Mattheo';

if(nickName === 'Matt'){
	console.log("Hello " + nickName);
}

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true
/* Syntax
	if(condition1){
		statement/s;
	}
	else if(condition2){
		statment/s;
	}
	else if(condition3){
		statment/s;
	}
	else if(conditionN){
		statment/s;
	}

*/

let numF = 1;

if(numE > 0){
	console.log("Hello");
}
else if(numF > 0){
	console.log("World");
}

// else clause
// executes a statement if all other conditions are not met.
/* Syntax:
	if(condition1){
		statement/s;
	}
	else if(condition2){
		statment/s;
	}
	else if(condition3){
		statment/s;
	}
	else if(conditionN){
		statment/s;
	}
	else{
		statement/s;
	}
*/

/*
	numE = -1;
	numF = 1;
*/
if(numE > 0){
	console.log("Hello");
}
else if(numF === 0){
	console.log("World");
}
else{
	console.log("Again");
}

let message = 'No message';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.';
	}
	else if(windSpeed >= 31 && windSpeed <= 60){
		return 'Tropical depression detected';
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	}
	else if(windSpeed >= 89 && windSpeed <=117){
		return 'Severe tropical strom detected';
	}
	else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

// Ternary operator
/* Syntax:
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? "true" : "false";
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// parseInt converts string values into numeric types
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name);
